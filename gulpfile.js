var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var minifycss = require('gulp-minify-css');
var jsmin = require('gulp-jsmin');
var image = require('gulp-image');
var autoprefixer = require('gulp-autoprefixer');
var $        = require('gulp-load-plugins')();

var sassPaths = [
    'node_modules/foundation-sites/scss',
    'node_modules/motion-ui/src'
];

gulp.task('scss', function() {
    gulp.src('./assets/scss/app.scss')
        .pipe(sass({
            includePaths: sassPaths
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(minifycss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./build/css'));

    gulp.src('./assets/scss/main.scss')
        .pipe(sass({
            includePaths: sassPaths
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sass())
        .pipe(minifycss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./build/css'));
});

gulp.task('scripts', function(){

    gulp.src([
            './node_modules/jquery/dist/jquery.js',
            './node_modules/foundation-sites/dist/foundation.js'
        ])
        .pipe(concat('app.js'))
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./build/js'));

    gulp.src('./assets/scripts/*.js')
        .pipe(concat('custom.js'))
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./build/js'));

});

gulp.task('images', function(){
    gulp.src('./assets/images/*')
        .pipe(image({
            pngquant: true,
            optipng: false,
            zopflipng: true,
            advpng: true,
            jpegRecompress: false,
            jpegoptim: true,
            mozjpeg: true,
            gifsicle: true,
            svgo: true
        }))
        .pipe(gulp.dest('./build/img/'));
});

gulp.task('copy', function() {
    return gulp.src('./assets/pages/*.html')
        .pipe(gulp.dest('./build'));
});

gulp.task('server', function() {
    return gulp.src('./build/')
        .pipe($.webserver({
            host: 'localhost',
            port: 1112,
            livereload: true,
            open: true
        }));
});

gulp.task('watch',['server','scss','scripts','images','copy'], function() {
    gulp.watch('./assets/scss/**/*.scss' , ['scss']);
    gulp.watch('./assets/scripts/*.js' , ['scripts']);
    gulp.watch('./assets/images/*' , ['images']);
    gulp.watch('./assets/pages/*' , ['copy']);
});

gulp.task('default', ['scss','scripts','images']);




