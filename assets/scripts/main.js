$(document).foundation();
$(document).ready( function(){
    $('a[href^="#"], a[href^="."]').click( function(){ // если в href начинается с # или ., то ловим клик
        var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 700); // анимируем скроолинг к элементу scroll_el
        }
        return false; // выключаем стандартное действие
    });
    $("#map").click( function(){
        $(".map").toggleClass("open");
    });
    $(".closemap").click( function(){
        $(".map").toggleClass("open");
    });
    ymaps.ready(init);
    var myMap;
    function init(){
        myMap = new ymaps.Map ("map_container", {
            center: [57.118955271498635,65.56568799999994],
            zoom: 16,
            controls: ['zoomControl']
        });
        myMap.behaviors.disable('scrollZoom');
        // Создает стиль
        // Создание метки
        var myPlacemark = new ymaps.Placemark([57.1185467339013,65.56616006878657], {
            hintContent: 'Office Creative'
        },{
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: 'img/icon_location.svg',
            // Размеры метки.
            iconImageSize: [46, 46],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-28, -56]
        });
        // Добавление метки на карту
        myMap.geoObjects.add(myPlacemark);
    }
});